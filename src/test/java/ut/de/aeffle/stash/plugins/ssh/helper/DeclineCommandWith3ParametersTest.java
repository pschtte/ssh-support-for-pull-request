package ut.de.aeffle.stash.plugins.ssh.helper;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;

import de.aeffle.stash.plugins.ssh.helper.DeclineCommand;
import de.aeffle.stash.plugins.ssh.helper.SshRequestCommand;
import de.aeffle.stash.plugins.ssh.helper.StashServiceHandler;

public class DeclineCommandWith3ParametersTest {
    private static final String PROJECT_KEY = "projectKey";
    private static final String REPOSITORY_SLUG = "repositorySlug";
    private static final int REPOSITORY_ID = 123;
    private static final long PULL_REQUEST_ID = 123L;
    private static final int PULL_REQUEST_VERSION = 123;
    
    private DeclineCommand declineCommand;

    @Before
    public void setUp() throws Exception {
        StashServiceHandler stashServiceHandler = new StashServiceHandler();
        PullRequestService pullRequestService = mock(PullRequestService.class);
        
        RepositoryService repositoryService = mock(RepositoryService.class);
        Repository repository = mock(Repository.class);
        PullRequest pullRequest = mock(PullRequest.class);
        
        when(pullRequestService.getById(REPOSITORY_ID, PULL_REQUEST_ID)).thenReturn(pullRequest);
        when(repositoryService.getBySlug(PROJECT_KEY, REPOSITORY_SLUG)).thenReturn(repository);
        when(repository.getId()).thenReturn(REPOSITORY_ID);
        when(pullRequest.getVersion()).thenReturn(PULL_REQUEST_VERSION);

        stashServiceHandler.add(pullRequestService);
        stashServiceHandler.add(repositoryService);
        
        String remoteCommand = SshRequestCommand.DECLINE_PULL_REQUEST + " " +
                PROJECT_KEY + " " +
                REPOSITORY_SLUG + " " + 
                PULL_REQUEST_ID;
        
        this.declineCommand = new DeclineCommand(stashServiceHandler, remoteCommand);
    }

    
    @Test
    public void test() {
        assertEquals(PULL_REQUEST_VERSION, declineCommand.getPullRequestVersion());
    }
    
    @Test
    public void testGetRepositoryId() {
        assertEquals(REPOSITORY_ID, declineCommand.getRepositoryId());
    }

    @Test
    public void testGetPullRequestId() {
        assertEquals(PULL_REQUEST_ID, declineCommand.getPullRequestId());
    }

}
