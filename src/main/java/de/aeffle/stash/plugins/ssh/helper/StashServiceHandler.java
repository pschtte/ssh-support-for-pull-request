package de.aeffle.stash.plugins.ssh.helper;

import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.repository.RepositoryService;

public class StashServiceHandler {
    
    private PullRequestService pullRequestService;
    private RepositoryService repositoryService;
    private NavBuilder navBuilder;

    public void add(PullRequestService pullRequestService) {
        this.pullRequestService = pullRequestService;
    }

    public PullRequestService getPullRequestService() {
        return pullRequestService;
    }

    public void add(RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }
    
    public RepositoryService getRepositoryService() {
        return repositoryService;
    }

    public void add(NavBuilder navBuilder) {
        this.navBuilder = navBuilder;
    }
    
    public NavBuilder getNavBuilder() {
        return navBuilder;
    }
}
