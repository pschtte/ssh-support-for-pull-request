package de.aeffle.stash.plugins.ssh.requestHandler;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.google.gson.Gson;

import de.aeffle.stash.plugins.ssh.exception.IncompletePullRequestInformationException;
import de.aeffle.stash.plugins.ssh.exception.UnspecifiedPullRequestSourceException;
import de.aeffle.stash.plugins.ssh.exception.UnspecifiedPullRequestTargetException;
import de.aeffle.stash.plugins.ssh.helper.SshShellHandle;
import de.aeffle.stash.plugins.ssh.helper.StashServiceHandler;
import de.aeffle.stash.plugins.ssh.json.JsonReviewer;
import de.aeffle.stash.plugins.ssh.json.JsonPullRequestData;
import de.aeffle.stash.plugins.ssh.json.JsonReference.MyRepository;


public class CreatePullRequestHandler extends AbstractRequestHandler {

    public CreatePullRequestHandler(String remoteCommand,
            SshShellHandle sshShellHandle,
            StashServiceHandler stashServiceHandler) {
        super(remoteCommand, sshShellHandle, stashServiceHandler);
    }

    
    @Override
    public void handleRequest() throws IOException {
        try {
            String jsonData = sshShellHandle.read();
            JsonPullRequestData pullRequestData = getPullRequestDataFromJson(jsonData);
            PullRequest pullRequest = tryPullRequestCreation(pullRequestData);
            
            sshShellHandle.writeOutput("Successfully created the pull-request");
            String pullRequestUrl = getPullRequestUrl(pullRequest);
            sshShellHandle.writeOutput("URL: " + pullRequestUrl);
        } 
        catch (Exception e) {
            handleError(e);
        }
        
    }


    public JsonPullRequestData getPullRequestDataFromJson(String pullRequestJsonData)
            throws IOException {
        /*
         * Gson API Documentation: https://sites.google.com/site/gson/gson-user-guide
         * 
         * STASH REST pull-request:
         * https://developer.atlassian.com/static/rest/stash/2.7.1/stash-rest.html#idp1497024
         */
        Gson gson = new Gson();

        return gson.fromJson(pullRequestJsonData, JsonPullRequestData.class);
    }


    private PullRequest tryPullRequestCreation(JsonPullRequestData pullRequest) throws IOException {
        checkForCompletePullRequestData(pullRequest); 
        writeToSshOutput(pullRequest);

        Repository fromRepo = getSourceRepository(pullRequest);
        Repository toRepo = getDestinationRepository(pullRequest);
        Set<String> reviewers = getReviewers(pullRequest);
 
        return createPullRequestInStash(pullRequest, fromRepo, toRepo, reviewers);

    }


    private PullRequest createPullRequestInStash(JsonPullRequestData pullRequest,
        Repository fromRepo, Repository toRepo, Set<String> reviewers) {

        PullRequestService pullRequestService = stashServiceHandler.getPullRequestService();
        
        return pullRequestService.create(pullRequest.title, pullRequest.description, 
                reviewers, 
                fromRepo, pullRequest.fromRef.id, 
                toRepo, pullRequest.toRef.id);
    }


    private void writeToSshOutput(JsonPullRequestData pullRequest) throws IOException {
        sshShellHandle.writeOutput("Received the following pull request:"); 
        sshShellHandle.writeOutput("Title: " + pullRequest.title);
        sshShellHandle.writeOutput("From: " + pullRequest.fromRef); 
        sshShellHandle.writeOutput("To: " + pullRequest.toRef);
    }


    private void checkForCompletePullRequestData(JsonPullRequestData pullRequest) {
        if ( !pullRequest.isInformationComplete() ) {
            throw new IncompletePullRequestInformationException();
        }
    }


    private Repository getDestinationRepository(JsonPullRequestData pullRequest) {
        MyRepository to = pullRequest.toRef.repository;
        RepositoryService repositoryService = stashServiceHandler.getRepositoryService();

        Repository toRepo = repositoryService.getBySlug(to.project.key, to.slug);
        if (toRepo == null) {
            throw new UnspecifiedPullRequestTargetException();
        }
        return toRepo;
    }


    private Repository getSourceRepository(JsonPullRequestData pullRequest) {
        MyRepository from = pullRequest.fromRef.repository;
        RepositoryService repositoryService = stashServiceHandler.getRepositoryService();

        Repository fromRepo = repositoryService.getBySlug(from.project.key, from.slug);
        if (fromRepo == null) {
            throw new UnspecifiedPullRequestSourceException();
        }
        return fromRepo;
    }


    private Set<String> getReviewers(JsonPullRequestData pullRequest) {
        Set<String> reviewers = new HashSet<String>();
        if (pullRequest.reviewers != null) {
            Iterator<JsonReviewer> it = pullRequest.reviewers.iterator();
            while (it.hasNext()) {
                JsonReviewer user = it.next();
                if (user != null) {
                    reviewers.add( user.toString() );
                }
            }
        }
        return reviewers;
    }


    private String getPullRequestUrl(PullRequest pullRequest) {
        Repository repository = pullRequest.getToRef().getRepository();
        return stashServiceHandler.getNavBuilder().
            project(repository.getProject().getKey()).
            repo(repository.getSlug()).
            pullRequest(pullRequest.getId()).
            buildAbsolute();
    }
    
}
