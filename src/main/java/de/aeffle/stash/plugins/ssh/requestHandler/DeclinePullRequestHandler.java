package de.aeffle.stash.plugins.ssh.requestHandler;

import java.io.IOException;

import com.atlassian.stash.pull.PullRequestService;
import de.aeffle.stash.plugins.ssh.helper.DeclineCommand;
import de.aeffle.stash.plugins.ssh.helper.SshShellHandle;
import de.aeffle.stash.plugins.ssh.helper.StashServiceHandler;


public class DeclinePullRequestHandler extends AbstractRequestHandler {

    public DeclinePullRequestHandler(String remoteCommand,
            SshShellHandle sshShellHandle,
            StashServiceHandler stashServiceHandler) {
        super(remoteCommand, sshShellHandle, stashServiceHandler);
    }


    @Override
    public void handleRequest() throws IOException {
        try {
            DeclineCommand declineCommand = new DeclineCommand(stashServiceHandler, remoteCommand);
            
            int repositoryId = declineCommand.getRepositoryId();
            long pullRequestId = declineCommand.getPullRequestId();
            int version = declineCommand.getPullRequestVersion();
            
            declinePullRequest(repositoryId, pullRequestId, version);
            
            sshShellHandle.writeOutput("Successfully declined the pull-request.");
        } 
        catch (Exception e) {
            handleError(e);
        }
        
    }

    private void declinePullRequest(int repositoryId, long pullRequestId, int version) {
        PullRequestService pullRequestService = stashServiceHandler.getPullRequestService();
        pullRequestService.decline(repositoryId, pullRequestId, version);
    }


}
