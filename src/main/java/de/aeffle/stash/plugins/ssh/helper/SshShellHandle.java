package de.aeffle.stash.plugins.ssh.helper;

import com.atlassian.stash.scm.ssh.ExitCodeCallback;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;


public class SshShellHandle {
    private static final int BUFFER_SIZE = 1024;

    private InputStream inputStream; 
    private OutputStream outputStream;
    private OutputStream errorStream;
    private ExitCodeCallback exitCodeCallback;
    
    public SshShellHandle(InputStream inputStream, 
            OutputStream outputStream, 
            OutputStream errorStream, 
            ExitCodeCallback exitCodeCallback) {
        this.inputStream = inputStream;
        this.outputStream = outputStream;
        this.errorStream = errorStream;
        this.exitCodeCallback = exitCodeCallback;
        
        setExitStatus(SshShellHandleExitStatus.SUCCESS);
    }
    
    public void writeOutput(String output) throws IOException {
    	writeToOutputStream(output, outputStream);
    }
    
    public void writeError(String output) throws IOException {
        writeToOutputStream(output, errorStream);
        setExitStatus(SshShellHandleExitStatus.ERROR);
    }
    
    private void writeToOutputStream(String output, OutputStream outputStream) throws IOException {
    	output += "\n";
        outputStream.write(output.getBytes());
        outputStream.flush();
    }
    
    public String read() throws IOException {
        return IOUtils.toString(inputStream);
    }
    
    public String oldRead() throws IOException {
        final byte[] buffer = new byte[BUFFER_SIZE];
        int n;
        String jsonData = new String();
    
        while (-1 != (n = inputStream.read(buffer))) {
            String bufferString = new String(buffer, 0, n);
            jsonData += bufferString;
        }
        
        return jsonData;
    }
    
    private void setExitStatus(SshShellHandleExitStatus exitStatus) {
        exitCodeCallback.onExit(exitStatus.getId());
    }
}
