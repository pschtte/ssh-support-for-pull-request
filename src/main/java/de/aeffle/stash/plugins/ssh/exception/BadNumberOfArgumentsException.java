package de.aeffle.stash.plugins.ssh.exception;

public class BadNumberOfArgumentsException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private static String errorMessage = "Not enough parameters provided.";
    
    public BadNumberOfArgumentsException() {
        super(errorMessage);
    }
    
}
