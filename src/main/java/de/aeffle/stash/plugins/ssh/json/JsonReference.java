package de.aeffle.stash.plugins.ssh.json;


/*
 * Reference:  Used for fromRef and toRef in PullRequest
 * 
 * Looks like this in json:
 * 'toRef': {'id': 'refs/heads/master', 
 *            'repository': { 'slug:'my-repo', 
 *                            'name': null, 
 *                            'project': {'key': 'PRJ'} } }, 
 */
public class JsonReference {
	public String id;
	public MyRepository repository;

	public class MyRepository {
		public String slug;
		public String name;

		public Project project;

		public class Project {
			public String key;
		}
	}
	
	public boolean infoComplete() {
		boolean infoComplete = false;
		if ( this.id != null
				&& this.repository != null
				&& this.repository.slug != null
				&& this.repository.project != null
				&& this.repository.project.key != null ) {
			infoComplete = true;
		}
		return infoComplete;
	}

	@Override
	public String toString() {
		return "(" + repository.project.key + "/"  + repository.slug + ") " + id;
	}
}