package de.aeffle.stash.plugins.ssh.requestHandler;

import java.io.IOException;
import com.atlassian.stash.pull.PullRequestService;
import com.google.gson.Gson;
import de.aeffle.stash.plugins.ssh.helper.CommentCommand;
import de.aeffle.stash.plugins.ssh.helper.SshShellHandle;
import de.aeffle.stash.plugins.ssh.helper.StashServiceHandler;
import de.aeffle.stash.plugins.ssh.json.JsonComment;


public class CommentPullRequestHandler extends AbstractRequestHandler {

    public CommentPullRequestHandler(String remoteCommand,
            SshShellHandle sshShellHandle,
            StashServiceHandler stashServiceHandler) {
        super(remoteCommand, sshShellHandle, stashServiceHandler);
    }


    @Override
    public void handleRequest() throws IOException {
        try {
            CommentCommand commentCommand = new CommentCommand(stashServiceHandler, remoteCommand);
            
            int repositoryId = commentCommand.getRepositoryId();
            long pullRequestId = commentCommand.getPullRequestId();
            
            String jsonData = sshShellHandle.read();
            JsonComment comment = getCommentFromJson(jsonData);
            
            commentPullRequest(repositoryId, pullRequestId, comment);
            
            sshShellHandle.writeOutput("Successfully commented on the pull-request.");
        } 
        catch (Exception e) {
            handleError(e);
        }
        
    }

    public JsonComment getCommentFromJson(String jsonData)
            throws IOException {
        // Gson API Documentation: https://sites.google.com/site/gson/gson-user-guide
        Gson gson = new Gson();
        return gson.fromJson(jsonData, JsonComment.class);
    }


    private void commentPullRequest(int repositoryId, long pullRequestId,
            JsonComment comment) {
        PullRequestService pullRequestService = stashServiceHandler.getPullRequestService();
        pullRequestService.addComment(repositoryId, pullRequestId, comment.toString());
    }


}
