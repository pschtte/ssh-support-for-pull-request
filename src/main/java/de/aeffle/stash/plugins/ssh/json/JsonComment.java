package de.aeffle.stash.plugins.ssh.json;

/*
 * STASH REST comment pull-request:
 * https://developer.atlassian.com/static/rest/stash/2.7.1/stash-rest.html#idp2620880
 * 
 * Looks like this in json:
 *  { 
 *   "text": "An insightful general comment on a pull request." 
 *  }
 */
public class JsonComment {
	String text;

	@Override
	public String toString() {
		return text;
	}
}