package de.aeffle.stash.plugins.ssh.exception;

public class InvalidPullRequestIdException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private static String errorMessage = "The pull-request id is not valid.";
    
    public InvalidPullRequestIdException() {
        super(errorMessage);
    }
}
