package de.aeffle.stash.plugins.ssh.exception;

public class IncompletePullRequestInformationException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private static String errorMessage = "The pull-request information not complete.";
    
    public IncompletePullRequestInformationException() {
        super(errorMessage );
    }
    
}
