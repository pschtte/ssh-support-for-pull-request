package de.aeffle.stash.plugins.ssh.requestHandler;

import com.atlassian.stash.scm.ssh.AbstractSshRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import de.aeffle.stash.plugins.ssh.helper.SshShellHandle;
import de.aeffle.stash.plugins.ssh.helper.StashServiceHandler;


public abstract class AbstractRequestHandler extends AbstractSshRequest {
    private static final Logger log = LoggerFactory
			.getLogger(AbstractRequestHandler.class);
	
    protected final StashServiceHandler stashServiceHandler;
    protected final SshShellHandle sshShellHandle;
    protected final String remoteCommand;
	

    public AbstractRequestHandler(String remoteCommand,
            SshShellHandle sshShellHandle,
            StashServiceHandler stashServiceHandler) {
        this.remoteCommand = remoteCommand;
        this.sshShellHandle = sshShellHandle;
        this.stashServiceHandler = stashServiceHandler;
    }

    protected void handleError(Exception e)
            throws IOException {
        String errorMessage2 = e.getClass().getSimpleName() + ":\n" + e.getMessage();
        sshShellHandle.writeError(errorMessage2);
        
        logStacktrace(e);
    }


    protected void logStacktrace(Exception e) {
    	StringWriter sw = new StringWriter();
    	PrintWriter pw = new PrintWriter(sw);
    	e.printStackTrace(pw);
    	String stacktrace = sw.toString();
    
    	log.error(stacktrace);
    }
}
